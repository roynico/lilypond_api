from lilypond_api.utils import MyValueError, octave_suffix, chromatic_scale_sharp, reg_ex_lilypond_name_compiled, \
    C_scale_reverse, convert_degree_diatonic_to_chromatic, octave_suffix_reverse, convert_alteration_to_delta, \
    reg_ex_midi_name_compiled, chromatic_scale_lilypond_sharp, convert_degree_chromatic_to_diatonic


class Pitch:
    # def __new__(cls, pitch, *args):
    #     if isinstance(pitch, type(cls)):
            ## return copy.deepcopy(pitch)
            # return super().__new__(cls)
        # else:
        #     return super().__new__(cls)

    def __init__(self, pitch):
        """
        create a note given pitch. Arg pitch can be one of the following:
        * a Pitch itself -> copy
        * a tuple (octave, degree)
        * a str. In this case, one tries to parse the str according to
           * midi notation
           * Lilypond notation

        defines :
            # self.octave
            # self.degree

        :param pitch:
        """

        try:
            # Initialization with other instance of Pitch
            # Already initialized in __new__()
            self._from_other_instance(other=pitch)
            return
        except Exception as e:
            pass

        try:
            # pitch is supposed to be (octave, degree)
            self._from_octave_and_degree(*pitch)
            return
        except Exception as e:
            pass

        try:
            # pitch is supposed to be a str in Lilypond notation
            self._from_lilypond_str(pitch)
            return
        except Exception as e:
            pass

        try:
            # pitch is supposed to be str in MIDI notation
            self._from_midi_str(pitch)
            # return
        except Exception as e:
            raise e

        # should not happen
        assert self.octave is not None and self.degree is not None

    def _from_other_instance(self, other):
        assert isinstance(other, Pitch)
        self.octave = other.octave
        self.degree = other.degree

    def _from_octave_and_degree(self, octave, degree):
        """
        degree is the chromatic degree of the note, relatively to C
        :param octave: int between 0 and 8
        :param degree: int between 1 and 12.
        :return:
        """
        if octave not in octave_suffix.keys():
            raise MyValueError("octave", tuple(octave_suffix.keys()), octave)

        if degree not in chromatic_scale_sharp.keys():
            raise MyValueError("degree", tuple(chromatic_scale_sharp.keys()), degree)

        self.octave = octave
        self.degree = degree

    def _from_lilypond_str(self, pitch):
        """
        pitch is supposed to be a str in lilypond notation
        :param pitch:
        :return:
        """
        lilypond_match = reg_ex_lilypond_name_compiled.match(pitch)
        try:
            degree_diatonic_name = lilypond_match.group(1)
            degree_diatonic = C_scale_reverse[degree_diatonic_name]
            degree = convert_degree_diatonic_to_chromatic.get(degree_diatonic)
            self.degree = degree

            octave = octave_suffix_reverse[lilypond_match.group(3)]
            self.octave = octave

            # Add the effect of the alteration
            alteration = convert_alteration_to_delta(lilypond_match.group(2))
            if alteration:
                self.__transpose__(alteration)
        except Exception as e:
            raise ValueError("Could not parse lilypond str:", pitch, e)

    def _from_midi_str(self, pitch):
        """
        pitch is supposed to be a str in MIDI notation (C4, E3, D2, ...)

        :param pitch:
        :return:
        """
        midi_match = reg_ex_midi_name_compiled.match(pitch)
        try:
            degree_diatonic_name = midi_match.group(1).lower()
            degree_diatonic = C_scale_reverse[degree_diatonic_name]
            degree = convert_degree_diatonic_to_chromatic.get(degree_diatonic)
            self.degree = degree
            octave = int(midi_match.group(3))
            self.octave = octave

            # Add the effect of the alteration
            alteration = convert_alteration_to_delta(midi_match.group(2))
            if alteration:
                self.__transpose__(alteration)

            assert self.octave in range(0, 9)
        except Exception as e:
            raise ValueError("Could not parse MIDI str:", pitch, e)

    def __transpose__(self, delta):
        """
        like __Add__, but modifies the object
        :return:
        """
        if not isinstance(delta, int):
            raise Exception('delta muss be integer')

        degree = ((self.degree + delta - 1) % 12) + 1
        octave = self.octave + (self.degree + delta - 1) // 12
        (self.octave, self.degree) = (octave, degree)

    def __add__(self, delta):
        """
        :param delta: int. This is the chromatic distance, to be added to the note
        :return:
        """
        new_note = Pitch(self)
        new_note.__transpose__(delta)
        return new_note

    def __sub__(self, delta):
        return self + (- delta)

    @property
    def name_midi(self):
        """
        use Sharp instead of Flat
        :return:
        """
        return "{deg}{oct}".format(
            deg=chromatic_scale_sharp[self.degree].upper(),
            oct=self.octave
        )

    @property
    def name_lilypond(self):
        """
        use Sharp instead of Flat
        :return:
        """
        return "{deg}{oct}".format(
            deg=chromatic_scale_lilypond_sharp.get(self.degree),
            oct=octave_suffix.get(self.octave)
        )

    def __str__(self):
        return self.name_lilypond

    def __hash__(self):
        return hash(self.name_lilypond)

    def __eq__(self, other):
        """
        other can be either another instance
        or, for convenience, a MIDI str representation
        :param other:
        :return:
        """

        if isinstance(other, Pitch):
            return self.octave == other.octave and self.degree == other.degree

        # deactivated because:
        # * one can not make it equl to both the midi and lilypond str and being consistent with __hash__
        # * there is no common notation for MIDI including the duration
        # * the notaion with duration for Lilypond would be OK,
        #    but it is ambiguous, because of cis = des, etc...
        # elif isinstance(other, str):
        #     return self.name_midi() == other
        else:
            return False

    @property
    def int_repr(self):
        """
        an int representing non ambiguously the pitch:
        between 0 and 107
        :return:
        """
        return self.octave * 12 + self.degree - 1

    def __lt__(self, other):
        """
        Implements the test pitch1 < pitch2
        :param other: either a string or a Pitch
        :return:
        """
        if isinstance(other, str):
            other = Pitch(other)

        return self.int_repr < other.int_repr

    def __gt__(self, other):
        """
        Implements the test pitch1 > pitch2
        :param other: either a string or a Pitch
        :return:
        """
        if isinstance(other, str):
            other = Pitch(other)
        return other.__lt__(self)

    def __le__(self, other):
        """
        Implements the test pitch1 <= pitch2
        :param other: either a string or a Pitch
        :return:
        """
        if isinstance(other, str):
            other = Pitch(other)
        return self.int_repr <= other.int_repr

    def __ge__(self, other):
        """
        Implements the test pitch1 >= pitch2
        :param other: either a string or a Pitch
        :return:
        """
        if isinstance(other, str):
            other = Pitch(other)
        return other.__le__(self)


class PitchWhite(Pitch):

    def __init__(self, pitch):
        """
        create a white note given pitch. Arg pitch can be one of the following:
        * a PitchWhite itself -> copy
        * a Pitch -> try to convert to diatonic
        * a tuple (octave, degree_diatonic)
        * a str. In this case, one tries to parse the str according to
           * midi notation
           * Lilypond notation

        defines :
            # self.octave
            # self.degree
            # self.degree_diatonic

        :param pitch:
        """
        # pitch a (octave, degree_diatonic)
        if isinstance(pitch, (tuple, list)):
            octave = pitch[0]
            degree_diatonic = pitch[1]
            degree = convert_degree_diatonic_to_chromatic.get(degree_diatonic)
            pitch = (octave, degree)

        super().__init__(pitch)
        try:
            self.degree_diatonic = convert_degree_chromatic_to_diatonic[self.degree]
        except KeyError:
            raise ValueError("Could not create PitchWhite with pitch={}".format(pitch))

    def __transpose__(self, delta):
        """
        like __Add__, but modifies the object
        :return:
        """
        raise Exception("__transpose__ not allowed for PitchWhite. Did you mean __transpose_diatonic__() ?")

    def __transpose_diatonic__(self, delta_diatonic):
        """
        like __Add__, but modifies the object
        :param delta_diatonic: int. diatonic delta
        :return:
        """
        if not isinstance(delta_diatonic, int):
            raise Exception('delta muss be integer')

        degree_diatonic = ((self.degree_diatonic + delta_diatonic - 1) % 7) + 1
        octave = self.octave + (self.degree_diatonic + delta_diatonic - 1) // 7
        degree = convert_degree_diatonic_to_chromatic[degree_diatonic]
        (self.octave, self.degree, self.degree_diatonic) = (octave, degree, degree_diatonic)

    def __add__(self, delta_diatonic):
        """
        :param delta_diatonic: int. This is the diatonic distance, to be added to the pitch
        :return:
        """
        new_pitch = PitchWhite(self)
        new_pitch.__transpose_diatonic__(delta_diatonic)
        return new_pitch

    def __sub__(self, delta_diatonic):
        return self + (- delta_diatonic)
