import re


class MyValueError(ValueError):
    def __init__(self, variable_name, allowed_values, provided_value):
        super().__init__()
        self.variable_name = variable_name
        self.provided_value = provided_value
        self.allowed_values = allowed_values

    def __str__(self):
        rep = "Value Error: {n} must be one of ({aut}). But {pro} provided.".format(
            n=self.variable_name,
            aut=self.allowed_values,
            # aut=", ".join(self.allowed_values),
            pro=self.provided_value
        )
        return rep


chromatic_scale_sharp = {1: 'c', 2: 'c#', 3: 'd', 4: 'd#', 5: 'e', 6: 'f', 7: 'f#', 8: 'g', 9: 'g#', 10: 'a', 11: 'a#', 12: 'b'}
chromatic_scale_sharp_reverse = {v: k for k, v in chromatic_scale_sharp.items()}
chromatic_scale_flat = {1: 'c', 2: 'db', 3: 'd', 4: 'eb', 5: 'e', 6: 'f', 7: 'gb', 8: 'g', 9: 'ab', 10: 'a', 11: 'bb', 12: 'b'}
chromatic_scale_flat_reverse = {v: k for k, v in chromatic_scale_flat.items()}

# Lilypond notation

chromatic_scale_lilypond_sharp = {1: 'c', 2: 'cis', 3: 'd', 4: 'dis', 5: 'e', 6: 'f', 7: 'fis', 8: 'g', 9: 'gis', 10: 'a', 11: 'ais', 12: 'b'}
chromatic_scale_lilypond_sharp_reverse = {v: k for k, v in chromatic_scale_lilypond_sharp.items()}
chromatic_scale_lilypond_flat = {1: 'c', 2: 'des', 3: 'd', 4: 'ees', 5: 'e', 6: 'f', 7: 'ges', 8: 'g', 9: 'aes', 10: 'a', 11: 'bes', 12: 'b'}
chromatic_scale_lilypond_flat_reverse = {v: k for k, v in chromatic_scale_lilypond_flat.items()}

C_scale = {1: 'c', 2: 'd', 3: 'e', 4: 'f', 5: 'g', 6: 'a', 7: 'b'}
C_scale_reverse = {v: k for k, v in C_scale.items()}
octave_suffix = {0: ",,,", 1: ",,", 2: ",", 3: '', 4: "'", 5: "''", 6: "'''", 7: "''''", 8: "'''''"}
octave_suffix_reverse = {v: k for k, v in octave_suffix.items()}
convert_degree_diatonic_to_chromatic = {1: 1, 2: 3, 3: 5, 4: 6, 5: 8, 6: 10, 7: 12}
convert_degree_chromatic_to_diatonic = {v: k for k, v in convert_degree_diatonic_to_chromatic.items()}


def convert_alteration_to_delta(alteration):
    """
    :param alteration: str. either "is" or "es", or "#" or "b". Or ''

    """
    if not isinstance(alteration, str):
        return 0
    elif not alteration:
        return 0
    elif 'is' in alteration:
        return alteration.count('s')
    elif 'es' in alteration:
        return - alteration.count('s')
    elif '#' in alteration:
        return alteration.count('#')
    elif 'b' in alteration:
        return - alteration.count('b')


reg_ex_midi_name = r"""^([CDEFGAB])([b#]?)([0-8])$"""
reg_ex_lilypond_name = r"""^([cdefgab])(es|is)?(,*|'*)$"""
reg_ex_midi_name_compiled = re.compile(reg_ex_midi_name)
reg_ex_lilypond_name_compiled = re.compile(reg_ex_lilypond_name)