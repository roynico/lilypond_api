__author__ = 'roy'

from lilypond_api.pitch import Pitch, PitchWhite

#todo is it usefull ?
white_notes = []  # c0 is 0

NOTE_STYLE_COLOR = "color"
NOTE_STYLE_SHAPE = "shape"


class Note:
    default_pitch_class = Pitch

    def __new__(cls, pitch=None, duration=None, copy_from=None, *kwargs):
        """
        Object is created either from a (pitch, duration) or from another instance
        :param pitch:
        :param duration:
        :param copy_from:
        :param args:
        """
        assert copy_from is None or pitch is None, \
            "Provide either pitch XOR copy_from"
        assert copy_from is not None or (pitch is not None and duration is not None), \
            "Provide either (pitch, duration) or copy_from"

        return super().__new__(cls)

    def __init__(self, pitch=None, duration=None, copy_from=None):
        """
        Object is created either from a (pitch, duration) or from another instance

        Pitch is either a Pitch object or a str, that must be parsed into a Pitch object

        overwrite duration if provided
        :param pitch:
        :param duration:
        :type copy_from:
        """
        if copy_from:
            self.pitch = self.default_pitch_class(pitch=copy_from.pitch)

            # overwrite duration if provided
            if duration is not None:
                self.duration = duration
            else:
                self.duration = copy_from.duration

        else:
            self.pitch = self.default_pitch_class(pitch=pitch)
            self.duration = duration

    @property
    def octave(self):
        return self.pitch.octave

    @property
    def degree(self):
        return self.pitch.degree

    def __transpose__(self, delta):
        self.pitch.__transpose__(delta=delta)

    def __add__(self, delta):
        """
        :param delta: int. This is the chromatic distance, to be added to the pitch of the note
        :return:
        """
        new_note = Note(copy_from=self)
        new_note.__transpose__(delta)
        return new_note

    def __sub__(self, delta):
        return self + (- delta)

    def name_lilypond(self):
        return "{n}{dur}".format(n=self.pitch.name_lilypond, dur=self.duration)

    def __str__(self):
        return self.name_lilypond()

    def __eq__(self, other):
        """
        Takes into account both the pitch and the duration
        :param other:
        :return:
        """
        if not isinstance(other, Note):
            raise TypeError(
                "'==' not supported between instances of 'Note' and '{}'".format(
                    type(other)
                )
            )

        return self.octave == other.octave and self.degree == other.degree and self.duration == other.duration

    def __lt__(self, other):
        """
        Implements the test note1 < note2
        Takes into account ONLY the pitch (ignores the duration)
        Note2 can also be a Pitch or a str representation of a Pitch

        :param other: either a Note or a Pitch (string)
        :return:
        """
        if isinstance(other, Note):
            other_pitch = other.pitch

        elif isinstance(other, (Pitch, str)):
            other_pitch = other
        else:
            raise TypeError(
                "'<' not supported between instances of 'Note' and '{}'".format(
                    type(other)
                )
            )
        return self.pitch < other_pitch

    def __gt__(self, other):
        """
        Implements the test note1 > note2
        Takes into account ONLY the pitch (ignores the duration)
        Note2 can also be a Pitch or a str representation of a Pitch

        :param other: either a Note or a Pitch (string)
        :return:
        """
        if isinstance(other, Note):
            other_pitch = other.pitch

        elif isinstance(other, (Pitch, str)):
            other_pitch = other
        else:
            raise TypeError(
                "'>' not supported between instances of 'Note' and '{}'".format(
                    type(other)
                )
            )
        return self.pitch > other_pitch

    def __le__(self, other):
        """
        Implements the test note1 <= note2
        Takes into account ONLY the pitch (ignores the duration)
        Note2 can also be a Pitch or a str representation of a Pitch

        :param other: either a Note or a Pitch (string)
        :return:
        """
        if isinstance(other, Note):
            other_pitch = other.pitch

        elif isinstance(other, (Pitch, str)):
            other_pitch = other
        else:
            raise TypeError(
                "'<=' not supported between instances of 'Note' and '{}'".format(
                    type(other)
                )
            )
        return self.pitch <= other_pitch

    def __ge__(self, other):
        """
        Implements the test note1 >= note2
        Takes into account ONLY the pitch (ignores the duration)
        Note2 can also be a Pitch or a str representation of a Pitch

        :param other: either a Note or a Pitch (string)
        :return:
        """
        if isinstance(other, Note):
            other_pitch = other.pitch

        elif isinstance(other, (Pitch, str)):
            other_pitch = other
        else:
            raise TypeError(
                "'>=' not supported between instances of 'Note' and '{}'".format(
                    type(other)
                )
            )
        return self.pitch >= other_pitch

    def __hash__(self):
        return hash(self.name_lilypond())


class NoteWhite(Note):
    default_pitch_class = PitchWhite

    @property
    def degree_diatonic(self):
        return self.pitch.degree_diatonic

    def __transpose_diatonic__(self, delta_diatonic):
        self.pitch.__transpose_diatonic__(delta_diatonic=delta_diatonic)

    def __add__(self, delta_diatonic):
        """
        :param delta_diatonic: int. This is the diatonic distance, to be added to the note
        :return:
        """
        new_note = NoteWhite(copy_from=self)
        new_note.__transpose_diatonic__(delta_diatonic)
        return new_note

    def __sub__(self, delta_diatonic):
        return self + (- delta_diatonic)


class Chord:
    def __new__(cls, pitches, duration, *args, **kwargs):
        try:
            for pitch in pitches:
                assert isinstance(pitch, Pitch)
        except:
            raise Exception("Could not initialize chord with data:", pitches)

        return super().__new__(cls)

    def __init__(self, pitches, duration, *args, **kwargs):
        self.pitches = pitches
        self.duration = duration

    @property
    def name_lilypond(self):
        return "< {notes} >{dur}".format(
            notes=' '.join([p.name_lilypond for p in self.pitches]),
            dur=self.duration
        )

    def __str__(self):
        return self.name_lilypond


def c_scale_generator(start="A0", end="C8", duration=None):
    """
    If duration is None -> scale of PitchWhite
    otherwise, scale of NoteWhite
    
    :param start: 
    :param end: 
    :param duration: 
    :return: 
    """
    if duration:
        start = NoteWhite(start, duration=duration)
        if end:
            end = NoteWhite(end, duration=duration)

    else:
        start = PitchWhite(start)
        if end:
            end = PitchWhite(end)

    current_note = start
    while (end is None) or (current_note <= end):
        yield current_note
        current_note = current_note + 1



class NoteWhiteWithStyle(NoteWhite):
    def __new__(cls, style=None, **kwargs):
        return super().__new__(cls, **kwargs)

    def __init__(self, style=None, **kwargs):
        """
        Object is created either from a (pitch, duration) or from another instance copy_from

        Pitch is either a Pitch object or a str, that must be parsed into a Pitch object

        overwrite duration if provided
        :param pitch:
        :param duration:
        :type copy_from:
        """
        super(NoteWhiteWithStyle, self).__init__(**kwargs)
        assert style in (None, True, False, NOTE_STYLE_COLOR, NOTE_STYLE_SHAPE)
        if not style:
            style = None
        elif style is True:
            style = NOTE_STYLE_COLOR
        self.style = style

    def __str__(self):
        note_without_style = super().__str__()
        if not self.style:
            return note_without_style
        elif self.style == NOTE_STYLE_COLOR:
            return r"""\override NoteHead #'color = #(x11-color "DarkRed") {n} \revert NoteHead #'color""".format(
                    n=note_without_style
            )
        elif self.style == NOTE_STYLE_SHAPE:
            return r"""\override NoteHead #'style = #'harmonic {n} \revert NoteHead #'style""".format(
                n=note_without_style
            )
