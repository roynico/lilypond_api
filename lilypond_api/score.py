from pathlib import Path
from subprocess import call

macro_auto_line_break = r"""
%% http://lsr.di.unimi.it/LSR/Item?id=838

%LSR completed by P.P.Schneider on Feb. 2014 for v2.18

#(define ((bars-per-line-engraver bar-list) context)
  (let* ((working-copy bar-list)
         (total (1+ (car working-copy))))
    `((acknowledgers
       (paper-column-interface
        . ,(lambda (engraver grob source-engraver)
             (let ((internal-bar (ly:context-property context 'internalBarNumber)))
               (if (and (pair? working-copy)
                        (= (remainder internal-bar total) 0)
                        (eq? #t (ly:grob-property grob 'non-musical)))
                   (begin
                     (set! (ly:grob-property grob 'line-break-permission) 'force)
                     (if (null? (cdr working-copy))
                         (set! working-copy bar-list)
                         (begin
                           (set! working-copy (cdr working-copy))))
                           (set! total (+ total (car working-copy))))))))))))
"""

default_blocs = {}

# Unused
default_blocs['version'] = r"""\version "2.16.2" """ + "\n"

default_blocs['preamble'] = macro_auto_line_break + "\n\n"

# Unused
default_blocs['header'] = r"""\header {
  composer = "Composer"
  title = "Title"
}
"""

default_blocs['paper'] = r"""\paper {
  indent = 0\cm
  top-margin = 20 \mm
  left-margin = 20 \mm
  right-margin = 20 \mm
  bottom-margin = 20\mm
  tagline = ""
}
"""

# Unused
default_blocs['layout'] = r"""\layout {
  \context {
    \Score
    skipBars = ##f
    \consists #(bars-per-line-engraver '(4))
  }
}
"""

# Unused
default_blocs['score'] = r"""\score {
      \new Staff{ \Melody}
  \layout {}
}
"""


def generate_bloc_version(version=None, **kwargs):
    """
    TODO: GET the version of lilypond

    :param version:
    :param kwargs:
    :return:
    """
    if not version:
        version = '2.16.2'

    return r"""\version "{0}" """.format(version) + "\n\n"


def generate_bloc_preamble(preamble, **kwargs):
    if not preamble:
        preamble = ''
    return '{0}\n{1}\n\n'.format(default_blocs['preamble'], preamble)


def generate_bloc_header(title=None, subtitle=None, composer=None, **kwargs):
    bloc_header = r"""\header {
    """
    if composer:
        bloc_header += """composer = "{0}" \n""".format(composer)
    if title:
        bloc_header += """title = "{0}" \n""".format(title)
    if subtitle:
        bloc_header += """subtitle = "{0}" \n""".format(subtitle)
    bloc_header += "\n}\n\n"

    return bloc_header


def generate_bloc_paper(**kwargs):
    return default_blocs['paper']


def generate_bloc_score(
        keys='treble',
        beats_per_bar=4,
        reduce_staff_staff_spacing=False,
        **kwargs
):
    bloc_score = r"""\score {""" + '\n'
    time_signature = generate_time_signature(beats_per_bar=beats_per_bar)

    if keys in ('treble', 'bass'):
        bloc_score += r"""\new Staff{{ {time} {clef}\MelodyA}}""".format(
            clef=generate_clef(keys=keys),
            time=time_signature
        ) + '\n'
    elif keys in ('double', 'piano'):
        # bloc_score += r"""\new PianoStaff{\autochange \Melody}""" + '\n'
        if reduce_staff_staff_spacing:
            spacing = r"""
    \with {
    \override StaffGrouper.staff-staff-spacing = #'( (padding . +0.5) )
    }"""
        else:
            spacing = ""

        bloc_score += r"""\new PianoStaff
        {spacing}
  <<
  \new Staff = "up" {{
    {time}\clef treble
    \autoChange{{\MelodyA}}
  }}
  \new Staff = "down" {{
   \clef bass
    \MelodyB
  }}
  >>
""".format(
            time=time_signature,
            spacing=spacing,
        ) + '\n'
    else:
        raise Exception("keys must be one of ('bass', 'treble', 'piano', 'double')")
    bloc_score += r"    }"

    return bloc_score


def generate_bloc_layout(bars_per_line=4, **kwargs):
    bloc_layout = r"""\layout {
  \context {
    \Score
    skipBars = ##f
    """
    if bars_per_line:
        bloc_layout += r"""\consists #(bars-per-line-engraver '({0}))""".format(bars_per_line) + '\n'
    bloc_layout += "   }\n}\n\n"

    return bloc_layout


def generate_time_signature(beats_per_bar):
    if int(beats_per_bar) == beats_per_bar:  # integer number of beats, like 3/4, 4/4, 5/4
        time_sig = r"\time {}/4".format(beats_per_bar)
    elif int(2 * beats_per_bar) == 2 * beats_per_bar:  # half-integer number of beats, like 4/8, 5/8, 7/8
        time_sig = r"\time {}/8".format(int(2*beats_per_bar))
    else:
        raise Exception("Only integer or half-integer beats_per_bar allowed")
    return time_sig


def generate_clef(keys):
    if keys == "treble":
        clef = '\\clef "treble"\n'
    elif keys == "bass":
        clef = '\\clef "bass"\n'
    elif keys in ('double', 'piano'):
        clef = ''
    else:
        raise Exception("keys must be one of ('bass', 'treble', 'piano', 'double')")

    return clef


numbering = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F'}


def generate_bloc_melody(melody, keys, **kwargs):
    """
    If keys = 'piano' or 'double' -> melody must be a pair of melodies, one for each staff
    :param melody:
    :param keys:
    :param kwargs:
    :return:
    """

    if keys in ('double', 'piano'):
        try:
            assert len(melody) == 2
            assert all(isinstance(melody[n], (tuple, list)) for n in (0, 1))
        except Exception as e:
            raise Exception("for keys in ('double', 'piano'), melody must be a pair of melodies")

    else:
        melody = (melody,)

    melody_source = []
    for n in range(0, len(melody)):
        melody_source.append("""Melody{num}= {{\n {melo} \n}}\n\n""".format(num=numbering[n], melo=' '.join(
            str(note) for note in melody[n])))

    return '\n'.join(melody_source)


def generate_score_source(
        melody,
        filename,
        output_dir=None,
        version=None,
        preamble=None,
        title=None,
        subtitle=None,
        composer=None,
        bars_per_line=4,
        beats_per_bar=4,
        reduce_staff_staff_spacing=False,
        keys='treble',
        **kwargs
):
    """
    Generate a ly source file from a melody (with line breaks), and metadata
    Store in filename

    :param melody:
    :param filename:
    :param version:
    :param preamble:
    :param title:
    :param subtitle:
    :param composer:
    :param bars_per_line:
    :param keys:
    :return:
    """
    score_source = generate_bloc_version(version=version)
    score_source += generate_bloc_preamble(preamble=preamble)
    score_source += generate_bloc_header(title=title, subtitle=subtitle, composer=composer)
    score_source += generate_bloc_paper()
    score_source += generate_bloc_layout(bars_per_line=bars_per_line)
    score_source += generate_bloc_melody(melody=melody, keys=keys)
    score_source += generate_bloc_score(
        keys=keys,
        beats_per_bar=beats_per_bar,
        reduce_staff_staff_spacing=reduce_staff_staff_spacing
    )

    if output_dir:
        filename = Path(output_dir) / filename
    with open(filename, 'w') as outfile:
        outfile.write(score_source)


def generate_score(filename_source, output_dir=None, source_dir=None):
    args = ["lilypond"]
    if output_dir:
        args.extend(['--output', output_dir])
    if source_dir:
        filename_source = Path(source_dir) / filename_source

    args.append(filename_source)
    call(args)
