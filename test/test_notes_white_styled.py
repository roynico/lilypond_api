from lilypond_api.notes import NoteWhite, NoteWhiteWithStyle, NOTE_STYLE_COLOR, NOTE_STYLE_SHAPE


def test_basic():
    # Good calls
    ns = NoteWhiteWithStyle(pitch=(3, 5), duration=4, style=None)
    assert ns.octave == 3
    assert ns.degree_diatonic == 5
    assert ns.style == None

    ns = NoteWhiteWithStyle(pitch=(3, 4), duration=4, style=NOTE_STYLE_COLOR)
    assert ns.octave == 3
    assert ns.degree_diatonic == 4
    assert ns.style == NOTE_STYLE_COLOR

    ns = NoteWhiteWithStyle(pitch=(2, 4), duration=4, style=True)
    assert ns.octave == 2
    assert ns.degree_diatonic == 4
    assert ns.style == NOTE_STYLE_COLOR

    ns = NoteWhiteWithStyle(pitch=(1, 6), duration=4, style=NOTE_STYLE_SHAPE)
    assert ns.octave == 1
    assert ns.degree_diatonic == 6
    assert ns.style == NOTE_STYLE_SHAPE

    ns = NoteWhiteWithStyle(pitch=(1, 6), duration=4, style=False)
    assert ns.octave == 1
    assert ns.degree_diatonic == 6
    assert ns.style is None


def test_copy_from():
    n = NoteWhite(pitch=(3, 5), duration=4)
    ns = NoteWhiteWithStyle(copy_from=n, style=None)
    assert ns.octave == 3
    assert ns.degree_diatonic == 5
    assert ns.style == None

    ns = NoteWhiteWithStyle(copy_from=n, style=NOTE_STYLE_SHAPE)
    assert ns.octave == 3
    assert ns.degree_diatonic == 5
    assert ns.style == NOTE_STYLE_SHAPE

    ns = NoteWhiteWithStyle(copy_from=n, style=True)
    assert ns.style == NOTE_STYLE_COLOR
