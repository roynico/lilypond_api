import pytest

from lilypond_api.pitch import Pitch


def test_pitch_basic():
    # Good calls
    assert Pitch(pitch=(3, 5)).octave == 3
    assert Pitch(pitch=(3, 5)).degree == 5
    assert Pitch('c').degree == 1
    assert Pitch("e'").degree == 5
    assert Pitch('b,').degree == 12
    assert Pitch('b,').degree == 12

    assert Pitch('C5').degree == 1
    assert Pitch('F3').degree == 6

    assert Pitch('A2').degree == 10

    for pitch_name in ('A2', 'b,', "e'", (3, 5)):
        x = Pitch(pitch_name)
        y = Pitch(x)
        assert x.octave == y.octave
        assert x.degree == y.degree


def test_pitch_alteration():
    assert Pitch('cis').degree == 2
    assert Pitch('dis').degree == 4
    assert Pitch('gis').degree == 9

    assert Pitch('C#4').degree == 2
    assert Pitch('A#4').degree == 11
    assert Pitch('D#4').degree == 4

    assert Pitch('ees').degree == Pitch('dis').degree
    assert Pitch('F#5').degree == Pitch('Gb6').degree

    for pitch_name in ('A#2', 'bes,', "eis'", (2, 6)):
        x = Pitch(pitch_name)
        y = Pitch(x)
        assert x.octave == y.octave
        assert x.degree == y.degree


def test_name():
    assert Pitch('C#4').name_midi == "C#4"
    assert Pitch('Db4').name_midi == "C#4"
    assert Pitch('C#4').name_lilypond  == "cis'"
    assert Pitch('Db4').name_lilypond  == "cis'"

    assert str(Pitch('D#3')) == "dis"


def test_add():
    assert (Pitch('D#4') + 4).degree == 8
    assert (Pitch('D#4') - 4).degree == 12
    assert (Pitch('D#4') + 12).octave == 5
    assert (Pitch('D#4') - 4).octave == 3


def test_note_alteration_octave_crossing():
    assert Pitch('Cb4').octave == 3
    assert Pitch('B#4').octave == 5
    assert Pitch('ces').octave == 2
    assert Pitch('bis').octave == 4


def test_copy():
    # Good calls
    n = Pitch((3, 5))
    m = Pitch(n)
    assert n.degree == m.degree and n.octave == m.octave
    assert n is not m


def test_pitch_bad_calls():
    with pytest.raises(Exception) as e:
        Pitch((4, 13))
        print(e)

    with pytest.raises(ValueError) as e:
        Pitch('W#4')
        print(e)

    with pytest.raises(ValueError) as e:
        Pitch('a#4')
        print(e)

    with pytest.raises(ValueError) as e:
        Pitch('Ees')
        print(e)


def test_name_midi():
    assert Pitch('G4').name_midi == 'G4'
    assert Pitch("g'").name_midi == 'G4'
    assert Pitch('G#4').name_midi == 'G#4'
    assert Pitch("Ab4").name_midi == 'G#4'

    assert Pitch('Cb4').name_midi == "B3"
    assert Pitch("B3").name_midi == "B3"
    assert Pitch('ces').name_midi == "B2"
    assert Pitch('B2').name_midi == "B2"
    assert Pitch('bis').name_midi == "C4"
    assert Pitch('C4').name_midi == "C4"


def test_name_lilypond():
    assert Pitch('G4').name_lilypond == "g'"
    assert Pitch("g'").name_lilypond == "g'"
    assert Pitch('G#4').name_lilypond == "gis'"
    assert Pitch("Ab4").name_lilypond == "gis'"

    assert Pitch('Cb4').name_lilypond == "b"
    assert Pitch("B3").name_lilypond == "b"
    assert Pitch('ces').name_lilypond == "b,"
    assert Pitch('B2').name_lilypond == "b,"
    assert Pitch('bis').name_lilypond == "c'"
    assert Pitch('C4').name_lilypond == "c'"


def test_eq():
    assert Pitch('G4') == Pitch("g'")
    assert Pitch('G4') != Pitch("g")
    assert Pitch('G#4') == Pitch("Ab4")
    assert Pitch('Cb4') == Pitch("B3")
    assert Pitch('ces') == Pitch("B2")
    assert Pitch('bis') == Pitch("C4")

    # Equality with str, either MIDI or LiLYPOND is DEACTIVATED
    # eq with Lilipond str
    assert Pitch('A3') != "a"
    assert Pitch('ais,') != "ais,"
    assert Pitch('D#2') != "ees,"
    assert Pitch('ees,') != "D#2"
    #
    # eq with MIDI str
    assert Pitch('F#5') != "F#5'"


def test_pitch_int():
    assert Pitch("C0").int_repr == 0
    assert Pitch("C1").int_repr == 12
    assert Pitch("C#1").int_repr == 13
    assert Pitch("C4").int_repr == 48
    assert Pitch("D4").int_repr == 50


def test_compare():
    assert Pitch("C4") < Pitch("C5")
    assert Pitch("C4") < Pitch("C#4")
    assert Pitch("C4") < Pitch("D4")
    assert not (Pitch("C4") < Pitch("C4"))
    assert not (Pitch("C4") < Pitch("F3"))
    assert not (Pitch("C4") < Pitch("B3"))

    assert Pitch("C5") > Pitch("C4")
    assert Pitch("C#4") > Pitch("C4")
    assert Pitch("D4") > Pitch("C4")
    assert not (Pitch("C4") > Pitch("C4"))
    assert not (Pitch("F3") > Pitch("C4"))
    assert not (Pitch("B3") > Pitch("C4"))


def test_compare_2():
    assert Pitch("C4") <= Pitch("C5")
    assert Pitch("C4") <= Pitch("C#4")
    assert Pitch("C4") <= Pitch("D4")
    assert Pitch("C4") <= Pitch("C4")
    assert Pitch("G#6") <= Pitch("G#6")
    assert not (Pitch("C4") <= Pitch("F3"))
    assert not (Pitch("C4") <= Pitch("B3"))

    assert Pitch("C5") >= Pitch("C4")
    assert Pitch("C#4") >= Pitch("C4")
    assert Pitch("D4") >= Pitch("C4")
    assert Pitch("C4") >= Pitch("C4")
    assert Pitch("G#6") >= Pitch("G#6")
    assert not (Pitch("F3") >= Pitch("C4"))
    assert not (Pitch("B3") >= Pitch("C4"))


def test_ordering():
    origin_list = [Pitch("C4"), Pitch("D4"), Pitch("G3"), Pitch("F#3"), Pitch("C#4")]
    sorted_list = [Pitch("F#3"), Pitch("G3"), Pitch("C4"), Pitch("C#4"), Pitch("D4")]
    assert sorted(origin_list) == sorted_list

    origin_list = [Pitch("C7"), Pitch("D5"), Pitch("G3"), Pitch("F#2"), Pitch("C#2")]
    sorted_list = [Pitch("C#2"), Pitch("F#2"), Pitch("G3"),  Pitch("D5"), Pitch("C7"),]
    assert sorted(origin_list) == sorted_list


def test_min_max():
    notes_list = [Pitch("C4"), Pitch("D4"), Pitch("G3"), Pitch("F#3"), Pitch("C#4")]
    assert min(notes_list) == Pitch("F#3")
    assert max(notes_list) == Pitch("D4")


def test_in():
    pitches_list = [Pitch("C4"), Pitch("D4"), Pitch("G3"), Pitch("F#3"), Pitch("C#4")]
    assert Pitch("C4") in pitches_list
    assert Pitch("D#4") not in pitches_list

    # does not work with pure strings...
    with pytest.xfail() as e:
        assert "C4" in pitches_list
    with pytest.xfail() as e:
        assert "c'" in pitches_list
    with pytest.xfail() as e:
        assert "G3" in pitches_list
    with pytest.xfail() as e:
        assert "g" in pitches_list
    with pytest.xfail() as e:
        assert "G#3" not in pitches_list
    with pytest.xfail() as e:
        assert "gis" not in pitches_list

    for p in pitches_list:
        with pytest.xfail() as e:
            assert p.name_midi() in pitches_list
        with pytest.xfail() as e:
            assert p.name_lilypond() in pitches_list

    pitches_set = set(pitches_list)
    assert Pitch("C4") in pitches_set
    assert Pitch("D#4") in pitches_set
    with pytest.xfail() as e:
        assert "C4" in pitches_set
    with pytest.xfail() as e:
        assert Pitch("D#4") in pitches_set
    for p in pitches_set:
        with pytest.xfail() as e:
            assert p.name_midi() in pitches_set
        with pytest.xfail() as e:
            assert p.name_lilypond() in pitches_set
