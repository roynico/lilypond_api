import pytest

from lilypond_api.notes import Note
from lilypond_api.pitch import Pitch


def test_note_basic():
    # Good calls
    assert Note(pitch=(3, 5), duration=7).octave == 3
    assert Note(pitch=(3, 5), duration=8).degree == 5
    assert Note(pitch=(3, 5), duration=8).duration == 8
    assert Note(pitch=(2, 4), duration=2).pitch == Pitch((2, 4))


def test_note_copy():
    # Good calls
    n = Note(pitch=(3, 5), duration=8)
    m = Note(copy_from=n)
    assert n.degree == m.degree and n.octave == m.octave and n.duration == m.duration
    assert n is not m

    # overwrite duration
    n = Note(pitch=(3, 5), duration=8)
    m = Note(copy_from=n, duration=2)
    assert n.degree == m.degree and n.octave == m.octave
    assert m.duration == 2 and n.duration == 8

    # copy is hard copy
    n = Note(pitch=(3, 5), duration=8)
    assert n.octave == 3 and n.degree ==5 and n.duration == 8
    m = Note(copy_from=n)
    assert m.octave == 3 and m.degree ==5 and m.duration == 8
    m.__transpose__(14)
    m.duration = 16
    assert m.octave == 4 and m.degree == 7 and m.duration == 16
    # original note is not modified
    assert n.octave == 3 and n.degree == 5 and n.duration == 8

    ### with add
    n = Note(pitch=(3, 5), duration=8)
    assert n.octave == 3 and n.degree == 5
    m = n + 1
    assert m.octave == 3 and m.degree == 6
    # original note is not modified
    assert n.octave == 3 and n.degree == 5



def test_note_bad_calls():
    with pytest.raises(Exception) as e:
        print(Note(pitch=(4, 15)))

    with pytest.raises(Exception) as e:
        print(Note(duration=8))

    with pytest.raises(Exception) as e:
        n = Note(pitch="A4", duration=8)
        print(Note(copy_from=n, pitch="C4"))


def test_note_alteration():
    assert Note(pitch='cis', duration=4).degree == 2
    assert Note(pitch='dis', duration=4).degree == 4
    assert Note(pitch='gis', duration=4).degree == 9

    assert Note(pitch='C#4', duration=4).degree == 2
    assert Note(pitch='A#4', duration=4).degree == 11
    assert Note(pitch='D#4', duration=4).degree == 4

    assert Note(pitch='ees', duration=4).degree == Note(pitch='dis', duration=4).degree
    assert Note(pitch='F#5', duration=4).degree == Note(pitch='Gb6', duration=4).degree



def test_transpose():
    n = Note(pitch='G#4', duration=4)
    assert n.pitch == Pitch('G#4')
    n.__transpose__(delta=2)
    assert n.pitch == Pitch('A#4')
    n.__transpose__(delta=-3)
    assert n.pitch == Pitch('G4')


def test_add():
    assert (Note(pitch='D#4', duration=4) + 4).degree == 8
    assert (Note(pitch='D#4', duration=2) - 4).degree == 12
    assert (Note(pitch='D#4', duration=8) + 12).octave == 5
    assert (Note(pitch='D#4', duration=16) - 4).octave == 3


def test_note_alteration_octave_crossing():
    assert Note('Cb4', duration=3).octave == 3
    assert Note('B#4', duration=2).octave == 5
    assert Note('ces', duration=1).octave == 2
    assert Note('bis', duration=4).octave == 4


def test_eq():
    assert Note(pitch='G4', duration=4) == Note(pitch="g'", duration=4)
    assert Note(pitch='G4', duration=2) != Note(pitch="g'", duration=4)
    assert Note(pitch='G4', duration=1) != Note(pitch="g", duration=1)

    assert Note(pitch='G#4', duration=8) == Note(pitch="Ab4", duration=8)
    assert Note(pitch='G#4', duration=8) != Note(pitch="Ab4", duration=4)

    assert Note(pitch='Cb4', duration=2) == Note(pitch="B3",duration=2)
    assert Note(pitch='Cb4', duration=2) != Note(pitch="B3",duration=4)

    with pytest.raises(TypeError) as e:
        assert Note(pitch='G4', duration=4) == "G4"


def test_compare():
    assert Note("C4", duration=4) < Note("C5", duration=4)
    assert Note("C4", duration=8) < Note("C5", duration=4)
    assert Note("C4", duration=4) < Note("C5", duration=8)

    assert Note("C4", duration=4) < Note("C#4", duration=4)
    assert Note("C4", duration=2) < Note("C#4", duration=4)
    assert Note("C4", duration=4) < Note("C#4", duration=2)

    assert Note("C4", duration=4) < Note("D4", duration=4)
    assert Note("C4", duration=8) < Note("D4", duration=4)
    assert Note("C4", duration=4) < Note("D4", duration=8)

    assert not (Note("C4", duration=4) < Note("C4", duration=4))
    assert not (Note("C4", duration=2) < Note("C4", duration=4))
    assert not (Note("C4", duration=4) < Note("C4", duration=2))

    assert not (Note("C4", duration=4) < Note("F3", duration=4))
    assert not (Note("C4", duration=8) < Note("F3", duration=4))
    assert not (Note("C4", duration=4) < Note("F3", duration=8))

    assert not (Note("C4", duration=4) < Note("B3", duration=4))
    assert not (Note("C4", duration=16) < Note("B3", duration=4))
    assert not (Note("C4", duration=4) < Note("B3", duration=16))


    assert Note("C5", duration=4) > Note("C4", duration=4)
    assert Note("C5", duration=8) > Note("C4", duration=4)
    assert Note("C5", duration=4) > Note("C4", duration=8)

    assert Note("C#4", duration=4) > Note("C4", duration=4)
    assert Note("C#4", duration=8) > Note("C4", duration=4)
    assert Note("C#4", duration=4) > Note("C4", duration=8)

    assert Note("D4", duration=4) > Note("C4", duration=4)
    assert Note("D4", duration=8) > Note("C4", duration=4)
    assert Note("D4", duration=4) > Note("C4", duration=8)

    assert not (Note("C4", duration=4) > Note("C4", duration=4))
    assert not (Note("C4", duration=8) > Note("C4", duration=4))
    assert not (Note("C4", duration=4) > Note("C4", duration=8))

    assert not (Note("F3", duration=4) > Note("C4", duration=4))
    assert not (Note("F3", duration=8) > Note("C4", duration=4))
    assert not (Note("F3", duration=4) > Note("C4", duration=8))

    assert not (Note("B3", duration=4) > Note("C4", duration=4))
    assert not (Note("B3", duration=2) > Note("C4", duration=4))
    assert not (Note("B3", duration=4) > Note("C4", duration=2))


def test_compare_with_pitch():
    assert Note("C4", duration=4) < Pitch("C5")
    assert Note("C4", duration=4) < "C5"

    assert Note("C4", duration=8) < Pitch("C#4")
    assert Note("C4", duration=8) < "C#4"

    assert Note("C4", duration=2) < Pitch("D4")
    assert Note("C4", duration=2) < "D4"

    assert not (Note("C4", duration=1) < Pitch("C4"))
    assert not (Note("C4", duration=1) < "C4")

    assert not (Note("C4", duration=4) < Pitch("F3"))
    assert not (Note("C4", duration=4) < "F3")

    assert not (Note("C4", duration=2) < Pitch("B3"))
    assert not (Note("C4", duration=2) < "B3")


    assert Note("C5", duration=4) > Pitch("C4")
    assert Note("C5", duration=4) > "C4"

    assert Note("C#4", duration=4) > Pitch("C4")
    assert Note("C#4", duration=4) > "C4"

    assert Note("D4", duration=4) > Pitch("C4")
    assert Note("D4", duration=4) > "C4"

    assert not (Note("C4", duration=4) > Pitch("C4"))
    assert not (Note("C4", duration=4) > "C4")

    assert not (Note("F3", duration=4) > Pitch("C4"))
    assert not (Note("F3", duration=4) > "C4")

    assert not (Note("B3", duration=4) > Pitch("C4"))
    assert not (Note("B3", duration=4) > "C4")


def test_compare_2():
    assert Note("C4", duration=8) <= Note("C5", duration=2)
    assert Note("C4", duration=8) <= Note("C#4", duration=2)
    assert Note("C4", duration=8) <= Note("D4", duration=2)
    assert Note("C4", duration=8) <= Note("C4", duration=2)
    assert Note("G#6", duration=8) <= Note("G#6", duration=2)
    assert not (Note("C4", duration=8) <= Note("F3", duration=2))
    assert not (Note("C4", duration=8) <= Note("B3", duration=2))

    assert Note("C5", duration=1) >= Note("C4", duration=4)
    assert Note("C#4", duration=2) >= Note("C4", duration=4)
    assert Note("D4", duration=8) >= Note("C4", duration=4)
    assert Note("C4", duration=4) >= Note("C4", duration=2)
    assert Note("G#6", duration=8) >= Note("G#6", duration=4)
    assert not (Note("F3", duration=1) >= Note("C4", duration=4))
    assert not (Note("B3", duration=1) >= Note("C4", duration=4))

def test_compare_with_pitch_2():
    assert Note("C4", duration=8) <= Pitch("C5")
    assert Note("C4", duration=8) <= "C5"

    assert Note("C4", duration=8) <= Pitch("C#4")
    assert Note("C4", duration=8) <= "C#4"

    assert Note("C4", duration=8) <= Pitch("D4")
    assert Note("C4", duration=8) <= "D4"

    assert Note("C4", duration=8) <= Pitch("C4")
    assert Note("C4", duration=8) <= "C4"

    assert Note("G#6", duration=8) <= Pitch("G#6")
    assert Note("G#6", duration=8) <= "G#6"

    assert not (Note("C4", duration=8) <= Pitch("F3"))
    assert not (Note("C4", duration=8) <= "F3")

    assert not (Note("C4", duration=8) <= Pitch("B3"))
    assert not (Note("C4", duration=8) <= "B3")


    assert Note("C5", duration=1) >= Pitch("C4")
    assert Note("C5", duration=1) >= "C4"

    assert Note("C#4", duration=2) >= Pitch("C4")
    assert Note("C#4", duration=4) >= "C4"

    assert Note("D4", duration=8) >= Pitch("C4")
    assert Note("D4", duration=1) >= "C4"

    assert Note("C4", duration=4) >= Pitch("C4")
    assert Note("C4", duration=8) >= "C4"

    assert Note("G#6", duration=8) >= Pitch("G#6")
    assert Note("G#6", duration=8) >= "G#6"

    assert not (Note("F3", duration=2) >= Pitch("C4"))
    assert not (Note("F3", duration=1) >= "C4")

    assert not (Note("B3", duration=8) >= Pitch("C4"))
    assert not (Note("B3", duration=4) >= "C4")


def test_ordering():
    origin_list = [Note("C4", duration=4), Note("D4", duration=4),
                   Note("G3", duration=4), Note("F#3", duration=4), Note("C#4", duration=4)]
    sorted_list = [Note("F#3", duration=4), Note("G3", duration=4),
                   Note("C4", duration=4), Note("C#4", duration=4), Note("D4", duration=4)]
    assert sorted(origin_list) == sorted_list

    origin_list = [Note("C7", duration=4), Note("D5", duration=4),
                   Note("G3", duration=4), Note("F#2", duration=4), Note("C#2", duration=4)]
    sorted_list = [Note("C#2", duration=4), Note("F#2", duration=4),
                   Note("G3", duration=4),  Note("D5", duration=4), Note("C7", duration=4),]
    assert sorted(origin_list) == sorted_list


def test_min_max():
    notes_list = [Note("C4", duration=3), Note("D4", duration=2),
                  Note("G3", duration=4), Note("F#3", duration=8), Note("C#4", duration=1)]
    assert min(notes_list) == Note("F#3", duration=8)
    assert max(notes_list) == Note("D4", duration=2)


def test_in():
    notes_list = [Note("C4", duration=4), Note("D4", duration=4),
                  Note("G3", duration=4), Note("F#3", duration=4), Note("C#4", duration=4)]
    assert Note("C4", duration=4) in notes_list
    assert Note("C4", duration=2) not in notes_list

    assert Note("D#4", duration=1) not in notes_list
    assert Note("D#4", duration=2) not in notes_list
    assert Note("D#4", duration=4) not in notes_list

    with pytest.xfail("== not defined for str !!") as e:
        assert "C4" in notes_list
    with pytest.xfail("== not defined for str !!") as e:
        assert "D4" in notes_list
    with pytest.xfail("== not defined for str !!") as e:
        assert "G3" in notes_list

    notes_set = set(notes_list)
    with pytest.xfail("== not defined for str !!") as e:
        assert "C4" in notes_set
    with pytest.xfail("== not defined for str !!") as e:
        assert "D4" in notes_set
