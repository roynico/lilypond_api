from lilypond_api.notes import Note, Chord
from lilypond_api.pitch import Pitch


def test_chord():
    n = Pitch('C4')
    m = Pitch('E4')
    l = Pitch('G4')
    ch = Chord(pitches=[n, m, l], duration=8)
    assert ch.duration == 8
    assert ch.pitches[0] == n
    assert str(ch).strip().startswith('<')
