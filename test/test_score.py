from lilypond_api.notes import Note, NoteWhite
from lilypond_api.score import *


def test_score_C_scale():
    my_melody = []
    octave = 4
    for degree_diatonic in range(1, 8):
        my_note = NoteWhite((octave, degree_diatonic), duration=4)
        my_melody.append(my_note)

    filename_source = 'test_score.ly'
    generate_score_source(melody=my_melody, filename=filename_source)
    generate_score(filename_source=filename_source)


def test_score_C_scale_2():
    my_melody = []
    C = NoteWhite("C4", duration=4)
    for delta_diatonic in range(0,15):
        my_note = C + delta_diatonic
        my_melody.append(my_note)

    filename_tmpl = 'test_score_C_Scale_{}.ly'
    for beats_per_bar in (3, 4, 3.5, 2.5):
        filename = filename_tmpl.format(beats_per_bar)
        generate_score_source(
            melody=my_melody,
            beats_per_bar=beats_per_bar,
            filename=filename
        )
        generate_score(filename_source=filename)
