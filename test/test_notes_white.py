import pytest

from lilypond_api.notes import NoteWhite, c_scale_generator


def test_basic():
    # Good calls
    assert NoteWhite(pitch=(3, 5), duration=4).octave == 3
    assert NoteWhite(pitch=(3, 5), duration=4).degree_diatonic == 5
    assert NoteWhite(pitch=(3, 5), duration=4).degree == 8
    assert NoteWhite('c', duration=4).degree == 1
    assert NoteWhite('c', duration=4).degree_diatonic == 1
    assert NoteWhite("e'", duration=4).degree == 5
    assert NoteWhite("e'", duration=4).degree_diatonic == 3
    assert NoteWhite('b,', duration=4).degree_diatonic == 7

    assert NoteWhite('C5', duration=4).degree == 1
    assert NoteWhite('C5', duration=4).degree_diatonic == 1
    assert NoteWhite('F3', duration=4).degree == 6
    assert NoteWhite('F3', duration=4).degree_diatonic == 4
    assert NoteWhite('A2', duration=4).degree == 10
    assert NoteWhite('A2', duration=4).degree_diatonic == 6


def test_transpose_diatonic():
    n = NoteWhite("F3", duration=4)
    n.__transpose_diatonic__(3)
    assert n == NoteWhite("B3", duration=4)
    n.__transpose_diatonic__(2)
    assert n == NoteWhite("D4", duration=4)


def test_add():
    assert (NoteWhite('D4', duration=1) + 4).degree_diatonic == 6
    assert (NoteWhite('D4', duration=2) - 4).degree_diatonic == 5
    assert (NoteWhite('D4', duration=4) - 4).degree == 8
    assert (NoteWhite('D4', duration=8) + 4).octave == 4
    assert (NoteWhite('D4', duration=8) - 4).octave == 3


def test_copy():
    n = NoteWhite(pitch=(4, 3), duration=8)
    m = NoteWhite(copy_from=n)
    assert n.degree == m.degree and n.degree_diatonic == m.degree_diatonic
    assert n.octave == m.octave and n.duration == m.duration
    assert n is not m


def test_bad_calls():
    with pytest.raises(ValueError) as e:
        print(NoteWhite('F#4', duration=4))

    with pytest.raises(ValueError) as e:
        print(NoteWhite('a#4', duration=8))

    with pytest.raises(ValueError) as e:
        print(NoteWhite('Ees', duration=8))


def test_eq():
    assert NoteWhite("G4", duration=4) == NoteWhite("g'", duration=4)
    assert NoteWhite("G4", duration=8) != NoteWhite("g", duration=8)
    assert NoteWhite("f,", duration=2) == NoteWhite("F2", duration=2)
    assert NoteWhite("E3", duration=1) == NoteWhite("E3", duration=1) + 0
    assert NoteWhite("D5", duration=8) != NoteWhite("D5", duration=8) + 1
    assert NoteWhite("C3", duration=4) == NoteWhite("C3", duration=4) + 0
    assert not (NoteWhite("C3", duration=4) != NoteWhite("C3", duration=4) + 0)


def test_duration():
    assert NoteWhite("C4", duration=4).duration == 4
    assert NoteWhite("D4", duration=8).duration == 8


def test_scale_2():
    def gen():
        s =("a", "b", "c", "d")
        for i in range(len(s)):
            yield s[i]
            i += 1

    for i in gen():
        print(i)
    for i in tuple(gen()):
        print(i)
    print(tuple(gen()))


def test_scale():
    for note in c_scale_generator(start="C4", end="C5", duration=4):
        print(note)
    for note in tuple(c_scale_generator(start="C4", end="F4", duration=4)):
        print(note, id(note), note.degree, id(note.pitch))

    assert tuple(c_scale_generator(start="C4", end="F4", duration=4)) == (NoteWhite("C4", duration=4), NoteWhite("D4", duration=4), NoteWhite("E4", duration=4), NoteWhite("F4", duration=4))
    assert tuple(c_scale_generator(start="B3", end="D4", duration=4)) == (NoteWhite("B3", duration=4), NoteWhite("C4", duration=4), NoteWhite("D4", duration=4))
    one_octave_scale = tuple(c_scale_generator(start="C4", end="C5", duration=4))
    assert one_octave_scale[0] == NoteWhite("C4", duration=4)
    assert one_octave_scale[-1] == NoteWhite("C5", duration=4)

    three_octave_scale = tuple(c_scale_generator(start="C3", end="C6", duration=4))
    assert len(three_octave_scale) == 22
    reduced_scale = [n for n in three_octave_scale if n <= "C5" and n >= "C4"]
    assert len(reduced_scale) == 8
    reduced_again = [n for n in reduced_scale if n < "C5"]
    assert len(reduced_again) == 7

    big_scale = tuple(c_scale_generator(start="C1", end="C8", duration=4))
    # There are many Cs
    assert len([n for n in big_scale if n.degree_diatonic == 1]) == 8

    # remove the Cs
    scale_wo_c = (n for n in big_scale if n.degree_diatonic != 1)
    # There are no more Cs
    assert len([n for n in scale_wo_c if n.degree_diatonic == 1]) == 0
