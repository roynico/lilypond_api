import pytest

from lilypond_api.notes import c_scale_generator
from lilypond_api.pitch import PitchWhite, Pitch


# from lilypond_api.utils import


def test_basic():
    # Good calls
    assert PitchWhite(pitch=(3, 5)).octave == 3
    assert PitchWhite(pitch=(3, 5)).degree_diatonic == 5
    assert PitchWhite(pitch=(3, 5)).degree == 8
    assert PitchWhite('c').degree == 1
    assert PitchWhite('c').degree_diatonic == 1
    assert PitchWhite("e'").degree == 5
    assert PitchWhite("e'").degree_diatonic == 3
    assert PitchWhite('b,').degree_diatonic == 7

    assert PitchWhite('C5').degree == 1
    assert PitchWhite('C5').degree_diatonic == 1
    assert PitchWhite('F3').degree == 6
    assert PitchWhite('F3').degree_diatonic == 4
    assert PitchWhite('A2').degree == 10
    assert PitchWhite('A2').degree_diatonic == 6


def test_transpose_diatonic():
    n = PitchWhite("F3")
    n.__transpose_diatonic__(3)
    assert n == PitchWhite("B3")
    n.__transpose_diatonic__(2)
    assert n == PitchWhite("D4")


def test_add():
    assert (PitchWhite('D4') + 4).degree_diatonic == 6
    assert (PitchWhite('D4') - 4).degree_diatonic == 5
    assert (PitchWhite('D4') - 4).degree == 8
    assert (PitchWhite('D4') + 4).octave == 4
    assert (PitchWhite('D4') - 4).octave == 3


def test_copy():
    n = PitchWhite(pitch=(4, 3))
    m = PitchWhite(pitch=n)
    assert n.degree == m.degree and n.degree_diatonic == m.degree_diatonic and n.octave == m.octave
    assert n is not m


def test_bad_calls():
    with pytest.raises(Exception) as e:
        print(PitchWhite('F#4'))

    with pytest.raises(Exception) as e:
        print(PitchWhite('a#4'))

    with pytest.raises(Exception) as e:
        print(PitchWhite('Ees'))


def test_eq():
    assert PitchWhite('G4') == PitchWhite("g'")
    assert PitchWhite('G4') != PitchWhite("g")
    assert PitchWhite('f,') == PitchWhite("F2")
    assert PitchWhite('E3') == PitchWhite("E3") + 0
    assert PitchWhite('D5') != PitchWhite("D5") + 1
    assert PitchWhite('C3') == PitchWhite("C3") + 0
    assert not (PitchWhite('C3') != PitchWhite("C3") + 0)


def test_convert_pitch_to_white():
    # convert possible
    assert PitchWhite(Pitch('G4')) == PitchWhite('G4')
    assert PitchWhite(Pitch('C3')) == PitchWhite('C3')
    # convert NOT possible
    with pytest.raises(ValueError) as e_info:
        assert PitchWhite(Pitch("gis'")) == PitchWhite('G#4')
    with pytest.raises(ValueError) as e_info:
        assert PitchWhite(Pitch("G#4")) == PitchWhite('G#4')


def test_scale():
    for note in c_scale_generator(start="C4", end="C5"):
        print(note)
    for note in tuple(c_scale_generator(start="C4", end="C5")):
        print(note)
    assert tuple(c_scale_generator(start="C4", end="F4")) == (PitchWhite("C4"), PitchWhite("D4"), PitchWhite("E4"), PitchWhite("F4"))
    assert tuple(c_scale_generator(start="B3", end="D4")) == (PitchWhite("B3"), PitchWhite("C4"), PitchWhite("D4"))
    one_octave_scale = tuple(c_scale_generator(start="C4", end="C5"))
    assert one_octave_scale[0] == PitchWhite("C4")
    assert one_octave_scale[-1] == PitchWhite("C5")

    three_octave_scale = tuple(c_scale_generator(start="C3", end="C6"))
    assert len(three_octave_scale) == 22
    reduced_scale = [n for n in three_octave_scale if n <= "C5" and n >= "C4"]
    assert len(reduced_scale) == 8
    reduced_again = [n for n in reduced_scale if n < "C5"]
    assert len(reduced_again) == 7

    big_scale = tuple(c_scale_generator(start="C1", end="C8"))
    # There are many Cs
    assert len([n for n in big_scale if n.degree_diatonic == 1]) == 8

    # remove the Cs
    scale_wo_c = (n for n in big_scale if n.degree_diatonic != 1)
    # There are no more Cs
    assert len([n for n in scale_wo_c if n.degree_diatonic == 1]) == 0
